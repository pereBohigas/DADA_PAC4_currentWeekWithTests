package edu.uoc.android.currentweek.utils;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

public class DateUtilsTest {

    Calendar calendar;
    Integer expectedWeekNumber;

    @Before
    public void setUp() throws Exception {

        calendar = Calendar.getInstance();
        expectedWeekNumber = calendar.get(Calendar.WEEK_OF_YEAR);

    }

    @Test
    public void isTheCurrentWeekNumber() throws Exception {

        DateUtils dateUtils = new DateUtils(calendar);

        // Test with the expected week number
        assertTrue(dateUtils.isTheCurrentWeekNumber(expectedWeekNumber));

        // Test with wrong week numbers
        assertFalse(dateUtils.isTheCurrentWeekNumber(expectedWeekNumber-1));
        assertFalse(dateUtils.isTheCurrentWeekNumber(expectedWeekNumber+1));

        // Test with values outside the normal range
        assertFalse(dateUtils.isTheCurrentWeekNumber(0));
        assertFalse(dateUtils.isTheCurrentWeekNumber(54));

    }
}